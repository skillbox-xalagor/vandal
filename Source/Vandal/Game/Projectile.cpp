// Fill out your copyright notice in the Description page of Project Settings.

#include "Projectile.h"
#include "Kismet/GameplayStatics.h"
#include "Perception/AISense_Damage.h"

// Sets default values
AProjectile::AProjectile()
{
	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	bReplicates = true;

	BulletCollisionSphere = CreateDefaultSubobject<USphereComponent>(TEXT("Collision Sphere"));

	BulletCollisionSphere->SetSphereRadius(16.f);

	BulletCollisionSphere->bReturnMaterialOnMove = true; // hit event return physMaterial

	BulletCollisionSphere->SetCanEverAffectNavigation(false); // collision doesn't affect navigation (P key in editor)

	RootComponent = BulletCollisionSphere;

	BulletMesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("Bullet Projectile Mesh"));
	BulletMesh->SetupAttachment(RootComponent);
	BulletMesh->SetCanEverAffectNavigation(false);

	BulletFX = CreateDefaultSubobject<UParticleSystemComponent>(TEXT("Bullet FX"));
	BulletFX->SetupAttachment(RootComponent);

	// BulletSound = CreateDefaultSubobject<UAudioComponent>(TEXT("Bullet Audio"));
	// BulletSound->SetupAttachment(RootComponent);

	BulletProjectileMovement = CreateDefaultSubobject<UProjectileMovementComponent>(TEXT("Bullet ProjectileMovement"));
	BulletProjectileMovement->UpdatedComponent = RootComponent;
	// BulletProjectileMovement->InitialSpeed = 1.f;
	// BulletProjectileMovement->MaxSpeed = 0.f;

	BulletProjectileMovement->bRotationFollowsVelocity = true;
	BulletProjectileMovement->bShouldBounce = true;
}

// Called when the game starts or when spawned
void AProjectile::BeginPlay()
{
	Super::BeginPlay();

	BulletCollisionSphere->OnComponentHit.AddDynamic(this, &AProjectile::BulletCollisionSphereHit);
	BulletCollisionSphere->OnComponentBeginOverlap.AddDynamic(this, &AProjectile::BulletCollisionSphereBeginOverlap);
	BulletCollisionSphere->OnComponentEndOverlap.AddDynamic(this, &AProjectile::BulletCollisionSphereEndOverlap);
}

// Called every frame
void AProjectile::Tick(const float DeltaTime)
{
	Super::Tick(DeltaTime);
}

void AProjectile::InitProjectile(const FProjectileInfo InitParam)
{
	// BulletProjectileMovement->InitialSpeed = InitParam.ProjectileInitSpeed;
	// BulletProjectileMovement->MaxSpeed = InitParam.ProjectileMaxSpeed;
	this->SetLifeSpan(InitParam.ProjectileLifeTime);
	if (InitParam.ProjectileStaticMesh)
	{
		InitVisualMeshProjectile_Multicast(InitParam.ProjectileStaticMesh, InitParam.ProjectileStaticMeshOffset);
	}
	else
	{
		BulletMesh->DestroyComponent();
	}

	if (InitParam.ProjectileTrailFx)
	{
		InitVisualTrailProjectile_Multicast(InitParam.ProjectileTrailFx, InitParam.ProjectileTrailFxOffset);
	}
	else
	{
		BulletFX->DestroyComponent();
	}
	InitVelocity_Multicast(InitParam.ProjectileInitSpeed, InitParam.ProjectileMaxSpeed);

	ProjectileSetting = InitParam;
}

void AProjectile::BulletCollisionSphereHit(UPrimitiveComponent* HitComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, FVector NormalImpulse,
                                           const FHitResult& Hit)
{
	if (OtherActor && Hit.PhysMaterial.IsValid())
	{
		const EPhysicalSurface MySurfaceType = UGameplayStatics::GetSurfaceType(Hit);

		if (ProjectileSetting.HitDecals.Contains(MySurfaceType))
		{
			UMaterialInterface* MyMaterial = ProjectileSetting.HitDecals[MySurfaceType];

			if (MyMaterial && OtherComp)
			{
				SpawnHitDecal_Multicast(MyMaterial, OtherComp, Hit);
			}
		}
		if (ProjectileSetting.HitFXs.Contains(MySurfaceType))
		{
			UParticleSystem* MyParticle = ProjectileSetting.HitFXs[MySurfaceType];
			if (MyParticle)
			{
				SpawnHitFx_Multicast(MyParticle, Hit);
			}
		}

		if (ProjectileSetting.HitSound)
		{
			SpawnHitSound_Multicast(ProjectileSetting.HitSound, Hit);
		}

		UTypes::AddEffectBySurfaceType(Hit.GetActor(), Hit.BoneName, ProjectileSetting.Effect, MySurfaceType);
	}

	UGameplayStatics::ApplyPointDamage(OtherActor, ProjectileSetting.ProjectileDamage, Hit.TraceStart, Hit, GetInstigatorController(), this, nullptr);
	UAISense_Damage::ReportDamageEvent(GetWorld(), Hit.GetActor(), GetInstigator(), ProjectileSetting.ProjectileDamage, Hit.Location, Hit.Location);
	// TODO: shotgun
	// trace, grenade

	ImpactProjectile();
}

// ReSharper disable once CppMemberFunctionMayBeStatic
void AProjectile::BulletCollisionSphereBeginOverlap(UPrimitiveComponent* /*OverlappedComponent*/, AActor* /*OtherActor*/, UPrimitiveComponent* /*OtherComp*/,
                                                    int32 /*OtherBodyIndex*/, bool /*bFromSweep*/, const FHitResult& /*SweepResult*/)
{
}

// ReSharper disable once CppMemberFunctionMayBeStatic
void AProjectile::BulletCollisionSphereEndOverlap(UPrimitiveComponent* /*OverlappedComponent*/, AActor* /*OtherActor*/, UPrimitiveComponent* /*OtherComp*/,
                                                  int32 /*OtherBodyIndex*/)
{
}

void AProjectile::ImpactProjectile()
{
	this->Destroy();
}

void AProjectile::InitVisualMeshProjectile_Multicast_Implementation(UStaticMesh* NewMesh, const FTransform MeshRelative)
{
	BulletMesh->SetStaticMesh(NewMesh);
	BulletMesh->SetRelativeTransform(MeshRelative);
}

void AProjectile::InitVisualTrailProjectile_Multicast_Implementation(UParticleSystem* NewTemplate, const FTransform TemplateRelative)
{
	BulletFX->SetTemplate(NewTemplate);
	BulletFX->SetRelativeTransform(TemplateRelative);
}

void AProjectile::SpawnHitDecal_Multicast_Implementation(UMaterialInterface* DecalMaterial, UPrimitiveComponent* OtherComp, const FHitResult HitResult)
{
	UGameplayStatics::SpawnDecalAttached(DecalMaterial, FVector(20.f), OtherComp, NAME_None, HitResult.ImpactPoint, HitResult.ImpactNormal.Rotation(),
	                                     EAttachLocation::KeepWorldPosition, 10.f);
}

void AProjectile::SpawnHitFx_Multicast_Implementation(UParticleSystem* FxTemplate, const FHitResult HitResult)
{
	UGameplayStatics::SpawnEmitterAtLocation(GetWorld(), FxTemplate, FTransform(HitResult.ImpactNormal.Rotation(), HitResult.ImpactPoint, FVector(1.0f)));
}

void AProjectile::SpawnHitSound_Multicast_Implementation(USoundBase* HitSound, const FHitResult HitResult)
{
	UGameplayStatics::PlaySoundAtLocation(GetWorld(), HitSound, HitResult.ImpactPoint);
}

void AProjectile::InitVelocity_Multicast_Implementation(const float InitSpeed, const float MaxSpeed)
{
	if (BulletProjectileMovement)
	{
		BulletProjectileMovement->Velocity = GetActorForwardVector() * InitSpeed;
		BulletProjectileMovement->MaxSpeed = MaxSpeed;
		BulletProjectileMovement->InitialSpeed = InitSpeed;
	}
}

void AProjectile::PostNetReceiveVelocity(const FVector& NewVelocity)
{
	if (BulletProjectileMovement)
	{
		BulletProjectileMovement->Velocity = NewVelocity;
	}
}
