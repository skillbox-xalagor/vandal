// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Vandal/Game/Projectile.h"
#include "Projectile_Grenade.generated.h"

/**
 * 
 */
UCLASS()
class VANDAL_API AProjectile_Grenade : public AProjectile
{
	GENERATED_BODY()

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	void TimerExplode(float DeltaTime);

	virtual void BulletCollisionSphereHit(UPrimitiveComponent* HitComp, AActor* OtherActor,
	                                      UPrimitiveComponent* OtherComp, FVector NormalImpulse,
	                                      const FHitResult& Hit) override;

	virtual void ImpactProjectile() override;

	UFUNCTION(Server, Reliable)
	void Explode_OnServer();

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Ability")
	bool TimerEnabled = false;
	float TimerToExplode = 0.0f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Grenade")
	float TimeToExplode = 5.0f;

	UFUNCTION(NetMulticast, Reliable)
	void SpawnExplodeFX_Multicast(UParticleSystem* FxTemplate);
	UFUNCTION(NetMulticast, Reliable)
	void SpawnExplodeSound_Multicast(USoundBase* ExplodeSound);
};
