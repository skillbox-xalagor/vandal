// Fill out your copyright notice in the Description page of Project Settings.

#include "Vandal/Game/Projectile_Grenade.h"
#include "DrawDebugHelpers.h"
#include "Kismet/GameplayStatics.h"
#include "Vandal/Interface/Vandal_IGameActor.h"

int32 DebugExplodeShow = 0;
FAutoConsoleVariableRef CVarExplodeShow(
	TEXT("Vandal.DebugExplode"),
	DebugExplodeShow,
	TEXT("Draw Debug for Explode"),
	ECVF_Cheat);

void AProjectile_Grenade::BeginPlay()
{
	Super::BeginPlay();
}

void AProjectile_Grenade::Tick(const float DeltaTime)
{
	Super::Tick(DeltaTime);
	if (HasAuthority())
	{
		TimerExplode(DeltaTime);
	}
}

void AProjectile_Grenade::TimerExplode(const float DeltaTime)
{
	if (TimerEnabled)
	{
		if (TimerToExplode > TimeToExplode)
		{
			Explode_OnServer();
		}
		else
		{
			TimerToExplode += DeltaTime;
		}
	}
}

void AProjectile_Grenade::BulletCollisionSphereHit(UPrimitiveComponent* HitComp, AActor* OtherActor,
                                                   UPrimitiveComponent* OtherComp, const FVector NormalImpulse, const FHitResult& Hit)
{
	if (!TimerEnabled)
	{
		Explode_OnServer();
	}

	Super::BulletCollisionSphereHit(HitComp, OtherActor, OtherComp, NormalImpulse, Hit);
}

void AProjectile_Grenade::ImpactProjectile()
{
	// Init Grenade
	TimerEnabled = true;
}

void AProjectile_Grenade::Explode_OnServer_Implementation()
{
	if (DebugExplodeShow)
	{
		DrawDebugSphere(
			GetWorld(), GetActorLocation(), ProjectileSetting.ProjectileMinRadiusDamage, 12, FColor::Green, false, 12.f);
		DrawDebugSphere(GetWorld(), GetActorLocation(), ProjectileSetting.ProjectileMaxRadiusDamage, 12, FColor::Red, false, 12.f);
	}

	TimerEnabled = false;
	if (ProjectileSetting.ExplodeFX)
	{
		SpawnExplodeFX_Multicast(ProjectileSetting.ExplodeFX);
	}
	if (ProjectileSetting.ExplodeSound)
	{
		SpawnExplodeSound_Multicast(ProjectileSetting.ExplodeSound);
	}

	const TArray<AActor*> IgnoredActor;
	UGameplayStatics::ApplyRadialDamageWithFalloff(GetWorld(),
	                                               ProjectileSetting.ExplodeMaxDamage,
	                                               ProjectileSetting.ExplodeMaxDamage * 0.2f,
	                                               GetActorLocation(),
	                                               ProjectileSetting.ProjectileMinRadiusDamage,
	                                               ProjectileSetting.ProjectileMaxRadiusDamage,
	                                               5,
	                                               nullptr, IgnoredActor, this, nullptr);
	this->Destroy();
}

void AProjectile_Grenade::SpawnExplodeFX_Multicast_Implementation(UParticleSystem* FxTemplate)
{
	UGameplayStatics::SpawnEmitterAtLocation(GetWorld(), FxTemplate, GetActorLocation(), GetActorRotation(), FVector(1.0f));
}

void AProjectile_Grenade::SpawnExplodeSound_Multicast_Implementation(USoundBase* ExplodeSound)
{
	UGameplayStatics::PlaySoundAtLocation(GetWorld(), ExplodeSound, GetActorLocation());
}
