// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "VandalGameMode.generated.h"

UCLASS(minimalapi)
class AVandalGameMode : public AGameModeBase
{
	GENERATED_BODY()

public:
	AVandalGameMode();

	static void PlayerCharacterDead();
};
