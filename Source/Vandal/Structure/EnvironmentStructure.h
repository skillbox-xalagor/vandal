// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Vandal/Game/StateEffect.h"
#include "Vandal/Interface/Vandal_IGameActor.h"
#include "EnvironmentStructure.generated.h"

UCLASS()
class VANDAL_API AEnvironmentStructure : public AActor, public IVandal_IGameActor
{
	GENERATED_BODY()

public:
	// Sets default values for this actor's properties
	AEnvironmentStructure();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;
	virtual bool ReplicateSubobjects(UActorChannel* Channel, FOutBunch* Bunch, FReplicationFlags* RepFlags) override;

public:
	// Called every frame
	virtual void Tick(float DeltaTime) override;


	virtual EPhysicalSurface GetSurfaceType() override;

	virtual TArray<UStateEffect*> GetAllCurrentEffects() override;
	UFUNCTION(BlueprintCallable, BlueprintNativeEvent)
	// ReSharper disable once CppHidingFunction
	void RemoveEffect(UStateEffect* RemoveEffect);
	virtual void RemoveEffect_Implementation(UStateEffect* RemoveEffect) override;
	UFUNCTION(BlueprintCallable, BlueprintNativeEvent)
	// ReSharper disable once CppHidingFunction
	void AddEffect(UStateEffect* NewEffect);
	virtual void AddEffect_Implementation(UStateEffect* NewEffect) override;

	//Effect
	UPROPERTY(Replicated, EditAnywhere, BlueprintReadWrite, Category = "Setting")
	TArray<UStateEffect*> Effects;
	UPROPERTY(ReplicatedUsing = EffectAdd_OnRep)
	UStateEffect* EffectAdd = nullptr;
	UPROPERTY(ReplicatedUsing = EffectRemove_OnRep)
	UStateEffect* EffectRemove = nullptr;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Debug")
	TArray<UParticleSystemComponent*> ParticleSystemEffects;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Setting")
	FVector OffsetEffect = FVector(0);

	UFUNCTION()
	void EffectAdd_OnRep();
	UFUNCTION()
	void EffectRemove_OnRep();

	UFUNCTION(Server, Reliable)
	void ExecuteEffectAdded_OnServer(UParticleSystem* ParticleSystem);
	UFUNCTION(NetMulticast, Reliable)
	void ExecuteEffectAdded_Multicast(UParticleSystem* ParticleSystem);

	UFUNCTION()
	void SwitchEffect(const UStateEffect* Effect, bool bIsAdd);
};
