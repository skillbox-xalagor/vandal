// Fill out your copyright notice in the Description page of Project Settings.


#include "EnvironmentStructure.h"
#include "Engine/ActorChannel.h"
#include "Kismet/GameplayStatics.h"
#include "Materials/MaterialInterface.h"
#include "Net/UnrealNetwork.h"
#include "Particles/ParticleSystemComponent.h"
#include "PhysicalMaterials/PhysicalMaterial.h"

// Sets default values
AEnvironmentStructure::AEnvironmentStructure()
{
	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	bReplicates = true;
}

// Called when the game starts or when spawned
void AEnvironmentStructure::BeginPlay()
{
	Super::BeginPlay();
}

// Called every frame
void AEnvironmentStructure::Tick(const float DeltaTime)
{
	Super::Tick(DeltaTime);
}

EPhysicalSurface AEnvironmentStructure::GetSurfaceType()
{
	EPhysicalSurface Result = SurfaceType_Default;
	const UStaticMeshComponent* MyMesh = Cast<UStaticMeshComponent>(GetComponentByClass(UStaticMeshComponent::StaticClass()));
	if (MyMesh)
	{
		const UMaterialInterface* MyMaterial = MyMesh->GetMaterial(0);
		if (MyMaterial)
		{
			Result = MyMaterial->GetPhysicalMaterial()->SurfaceType;
		}
	}
	return Result;
}

TArray<UStateEffect*> AEnvironmentStructure::GetAllCurrentEffects()
{
	return Effects;
}

void AEnvironmentStructure::RemoveEffect_Implementation(UStateEffect* RemoveEffect)
{
	Effects.Remove(RemoveEffect);

	if (!RemoveEffect->bIsAutoDestroyParticleEffect)
	{
		SwitchEffect(RemoveEffect, false);
		EffectRemove = RemoveEffect;
	}
}

void AEnvironmentStructure::AddEffect_Implementation(UStateEffect* NewEffect)
{
	Effects.Add(NewEffect);

	if (!NewEffect->bIsAutoDestroyParticleEffect)
	{
		SwitchEffect(NewEffect, true);
		EffectAdd = NewEffect;
	}
	else
	{
		if (NewEffect->ParticleEffect)
		{
			ExecuteEffectAdded_OnServer(NewEffect->ParticleEffect);
		}
	}
}

void AEnvironmentStructure::EffectAdd_OnRep()
{
	if (EffectAdd)
	{
		SwitchEffect(EffectAdd, true);
	}
}

void AEnvironmentStructure::EffectRemove_OnRep()
{
	if (EffectRemove)
	{
		SwitchEffect(EffectRemove, false);
	}
}

void AEnvironmentStructure::ExecuteEffectAdded_OnServer_Implementation(UParticleSystem* ExecuteFx)
{
	ExecuteEffectAdded_Multicast(ExecuteFx);
}

void AEnvironmentStructure::ExecuteEffectAdded_Multicast_Implementation(UParticleSystem* ExecuteFx)
{
	UTypes::ExecuteEffectAdded(ExecuteFx, this, OffsetEffect, NAME_None);
}

void AEnvironmentStructure::SwitchEffect(const UStateEffect* Effect, const bool bIsAdd)
{
	if (bIsAdd)
	{
		if (Effect && Effect->ParticleEffect)
		{
			const FName NameBoneToAttached = NAME_None;
			const FVector Loc = OffsetEffect;

			USceneComponent* MySceneComp = GetRootComponent();
			if (MySceneComp)
			{
				UParticleSystemComponent* NewParticleSystem = UGameplayStatics::SpawnEmitterAttached(Effect->ParticleEffect, MySceneComp,
				                                                                                     NameBoneToAttached, Loc, FRotator::ZeroRotator,
				                                                                                     EAttachLocation::SnapToTarget, false);
				ParticleSystemEffects.Add(NewParticleSystem);
			}
		}
	}
	else
	{
		if (ParticleSystemEffects.Num() > 0)
		{
			bool bIsFind = false;
			int32 i = 0;
			while (i < ParticleSystemEffects.Num() && !bIsFind)
			{
				if (ParticleSystemEffects[i]->Template && Effect->ParticleEffect && Effect->ParticleEffect == ParticleSystemEffects[i]->Template)
				{
					bIsFind = true;
					ParticleSystemEffects[i]->DeactivateSystem();
					ParticleSystemEffects[i]->DestroyComponent();
					ParticleSystemEffects.RemoveAt(i);
				}
				i++;
			}
		}
	}
}

bool AEnvironmentStructure::ReplicateSubobjects(UActorChannel* Channel, FOutBunch* Bunch, FReplicationFlags* RepFlags)
{
	bool Wrote = Super::ReplicateSubobjects(Channel, Bunch, RepFlags);

	for (int32 i = 0; i < Effects.Num(); i++)
	{
		if (Effects[i])
		{
			Wrote |= Channel->ReplicateSubobject(Effects[i], *Bunch, *RepFlags);
		}
	}
	return Wrote;
}

void AEnvironmentStructure::GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const
{
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);

	DOREPLIFETIME(AEnvironmentStructure, Effects);
	DOREPLIFETIME(AEnvironmentStructure, EffectAdd);
	DOREPLIFETIME(AEnvironmentStructure, EffectRemove);
}
