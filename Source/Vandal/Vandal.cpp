// Copyright Epic Games, Inc. All Rights Reserved.

#include "Vandal.h"
#include "Modules/ModuleManager.h"

IMPLEMENT_PRIMARY_GAME_MODULE(FDefaultGameModuleImpl, Vandal, "Vandal");

DEFINE_LOG_CATEGORY(LogVandal)
DEFINE_LOG_CATEGORY(LogVandalNetwork)