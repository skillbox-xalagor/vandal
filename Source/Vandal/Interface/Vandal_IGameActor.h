// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "UObject/Interface.h"
#include "Vandal/FuncLibrary/Types.h"
#include "Vandal/Game/StateEffect.h"
#include "Vandal_IGameActor.generated.h"

// This class does not need to be modified.
UINTERFACE(MinimalAPI)
class UVandal_IGameActor : public UInterface
{
	GENERATED_BODY()
};

class VANDAL_API IVandal_IGameActor
{
	GENERATED_BODY()

	// Add interface functions to this class. This is the class that will be inherited to implement this interface.
public:
	virtual EPhysicalSurface GetSurfaceType();
	virtual TArray<UStateEffect*> GetAllCurrentEffects();
	UFUNCTION(BlueprintCallable, BlueprintNativeEvent)
	// ReSharper disable once CppHiddenFunction
	void RemoveEffect(UStateEffect* RemoveEffect);
	UFUNCTION(BlueprintCallable, BlueprintNativeEvent)
	// ReSharper disable once CppHiddenFunction
	void AddEffect(UStateEffect* NewEffect);

	//inv
	UFUNCTION(BlueprintCallable, BlueprintNativeEvent)
	void DropWeaponToWorld(FDropItem DropItemInfo);
	UFUNCTION(BlueprintCallable, BlueprintNativeEvent)
	// ReSharper disable once CppUEBlueprintCallableFunctionUnused
	void DropAmmoToWorld(EWeaponType TypeAmmo, int32 Count);
};
