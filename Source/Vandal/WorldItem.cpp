// Fill out your copyright notice in the Description page of Project Settings.


#include "WorldItem.h"

// Sets default values
AWorldItem::AWorldItem()
{
	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
}

// Called when the game starts or when spawned
void AWorldItem::BeginPlay()
{
	Super::BeginPlay();
}

// Called every frame
void AWorldItem::Tick(const float DeltaTime)
{
	Super::Tick(DeltaTime);
}
