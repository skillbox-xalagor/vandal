// Fill out your copyright notice in the Description page of Project Settings.

#include "Vandal/FuncLibrary/Types.h"
#include "GameFramework/Character.h"
#include "Kismet/GameplayStatics.h"
#include "Vandal/Interface/Vandal_IGameActor.h"

void UTypes::AddEffectBySurfaceType(AActor* TakeEffectActor, const FName NameBoneHit, const TSubclassOf<UStateEffect> AddEffectClass,
                                    const EPhysicalSurface SurfaceType)
{
	if (SurfaceType != SurfaceType_Default && TakeEffectActor && AddEffectClass)
	{
		UStateEffect* MyEffect = Cast<UStateEffect>(AddEffectClass->GetDefaultObject());
		if (MyEffect)
		{
			bool bIsHavePossibleSurface = false;
			int8 i = 0;
			while (i < MyEffect->PossibleInteractSurface.Num() && !bIsHavePossibleSurface)
			{
				if (MyEffect->PossibleInteractSurface[i] == SurfaceType)
				{
					bIsHavePossibleSurface = true;
					bool bIsCanAddEffect = false;
					if (!MyEffect->bIsStackable)
					{
						TArray<UStateEffect*> CurrentEffects;
						IVandal_IGameActor* MyInterface = Cast<IVandal_IGameActor>(TakeEffectActor);
						if (MyInterface)
						{
							CurrentEffects = MyInterface->GetAllCurrentEffects();
						}

						if (CurrentEffects.Num() > 0)
						{
							int8 j = 0;
							while (j < CurrentEffects.Num() && !bIsCanAddEffect)
							{
								if (CurrentEffects[j]->GetClass() != AddEffectClass)
								{
									bIsCanAddEffect = true;
								}
								j++;
							}
						}
						else
						{
							bIsCanAddEffect = true;
						}
					}
					else
					{
						bIsCanAddEffect = true;
					}

					if (bIsCanAddEffect)
					{
						UStateEffect* NewEffect = NewObject<UStateEffect>(TakeEffectActor, AddEffectClass);
						if (NewEffect)
						{
							NewEffect->InitObject(TakeEffectActor, NameBoneHit);
						}
					}
				}
				i++;
			}
		}
	}
}

void UTypes::ExecuteEffectAdded(UParticleSystem* ExecuteFx, AActor* Target, const FVector Offset, const FName Socket)
{
	if (Target)
	{
		const FName SocketToAttached = Socket;
		const FVector Loc = Offset;
		const ACharacter* MyCharacter = Cast<ACharacter>(Target);
		if (MyCharacter && MyCharacter->GetMesh())
		{
			UGameplayStatics::SpawnEmitterAttached(ExecuteFx, MyCharacter->GetMesh(), SocketToAttached, Loc, FRotator::ZeroRotator,
			                                       EAttachLocation::SnapToTarget, false);
		}
		else
		{
			if (Target->GetRootComponent())
			{
				UGameplayStatics::SpawnEmitterAttached(ExecuteFx, Target->GetRootComponent(), SocketToAttached, Loc,
				                                       FRotator::ZeroRotator, EAttachLocation::SnapToTarget, false);
			}
		}
	}
}
