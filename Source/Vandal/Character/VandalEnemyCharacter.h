// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Character.h"
#include "Vandal/Interface/Vandal_IGameActor.h"
#include "VandalEnemyCharacter.generated.h"

UCLASS()
class VANDAL_API AVandalEnemyCharacter : public ACharacter, public IVandal_IGameActor
{
	GENERATED_BODY()

public:
	// Sets default values for this character's properties
	AVandalEnemyCharacter();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;
	virtual bool ReplicateSubobjects(UActorChannel* Channel, FOutBunch* Bunch, FReplicationFlags* RepFlags) override;

public:
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	// Called to bind functionality to input
	virtual void SetupPlayerInputComponent(UInputComponent* PlayerInputComponent) override;

	UFUNCTION(BlueprintCallable, BlueprintNativeEvent)
	// ReSharper disable once CppEnforceOverridingFunctionStyle
	// ReSharper disable once CppHidingFunction
	void RemoveEffect(UStateEffect* RemoveEffect);
	virtual void RemoveEffect_Implementation(UStateEffect* RemoveEffect) override;
	UFUNCTION(BlueprintCallable, BlueprintNativeEvent)
	// ReSharper disable once CppEnforceOverridingFunctionStyle
	// ReSharper disable once CppHidingFunction
	void AddEffect(UStateEffect* NewEffect);
	virtual void AddEffect_Implementation(UStateEffect* NewEffect) override;

	//Effect
	UPROPERTY(Replicated, EditAnywhere, BlueprintReadWrite, Category = "Setting")
	TArray<UStateEffect*> Effects;
	UPROPERTY(ReplicatedUsing = EffectAdd_OnRep)
	UStateEffect* EffectAdd = nullptr;
	UPROPERTY(ReplicatedUsing = EffectRemove_OnRep)
	UStateEffect* EffectRemove = nullptr;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Debug")
	TArray<UParticleSystemComponent*> ParticleSystemEffects;

	UFUNCTION()
	void EffectAdd_OnRep();
	UFUNCTION()
	void EffectRemove_OnRep();

	UFUNCTION(Server, Reliable)
	void ExecuteEffectAdded_OnServer(UParticleSystem* ParticleSystem);
	UFUNCTION(NetMulticast, Reliable)
	void ExecuteEffectAdded_Multicast(UParticleSystem* ParticleSystem);

	UFUNCTION()
	void SwitchEffect(const UStateEffect* Effect, bool bIsAdd);
};
