// Fill out your copyright notice in the Description page of Project Settings.


#include "Vandal/Character/VandalEnemyCharacter.h"
#include "Engine/ActorChannel.h"
#include "Kismet/GameplayStatics.h"
#include "Net/UnrealNetwork.h"
#include "Particles/ParticleSystemComponent.h"

// Sets default values
AVandalEnemyCharacter::AVandalEnemyCharacter()
{
	// Set this character to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
}

// Called when the game starts or when spawned
void AVandalEnemyCharacter::BeginPlay()
{
	Super::BeginPlay();
}

// Called every frame
void AVandalEnemyCharacter::Tick(const float DeltaTime)
{
	Super::Tick(DeltaTime);
}

// Called to bind functionality to input
void AVandalEnemyCharacter::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);
}

void AVandalEnemyCharacter::RemoveEffect_Implementation(UStateEffect* RemoveEffect)
{
	Effects.Remove(RemoveEffect);

	if (!RemoveEffect->bIsAutoDestroyParticleEffect)
	{
		SwitchEffect(RemoveEffect, false);
		EffectRemove = RemoveEffect;
	}
}

void AVandalEnemyCharacter::AddEffect_Implementation(UStateEffect* NewEffect)
{
	Effects.Add(NewEffect);

	if (!NewEffect->bIsAutoDestroyParticleEffect)
	{
		SwitchEffect(NewEffect, true);
		EffectAdd = NewEffect;
	}
	else
	{
		if (NewEffect->ParticleEffect)
		{
			ExecuteEffectAdded_OnServer(NewEffect->ParticleEffect);
		}
	}
}

void AVandalEnemyCharacter::EffectAdd_OnRep()
{
	if (EffectAdd)
	{
		SwitchEffect(EffectAdd, true);
	}
}

void AVandalEnemyCharacter::EffectRemove_OnRep()
{
	if (EffectRemove)
	{
		SwitchEffect(EffectRemove, false);
	}
}

void AVandalEnemyCharacter::ExecuteEffectAdded_OnServer_Implementation(UParticleSystem* ExecuteFx)
{
	ExecuteEffectAdded_Multicast(ExecuteFx);
}

void AVandalEnemyCharacter::ExecuteEffectAdded_Multicast_Implementation(UParticleSystem* ExecuteFx)
{
	UTypes::ExecuteEffectAdded(ExecuteFx, this, FVector(0), FName("spine_01"));
}

void AVandalEnemyCharacter::SwitchEffect(const UStateEffect* Effect, const bool bIsAdd)
{
	if (bIsAdd)
	{
		if (Effect && Effect->ParticleEffect)
		{
			const FName NameBoneToAttached = Effect->NameBone;
			const FVector Loc = FVector(0);

			USkeletalMeshComponent* MyMesh = GetMesh();
			if (MyMesh)
			{
				UParticleSystemComponent* NewParticleSystem = UGameplayStatics::SpawnEmitterAttached(Effect->ParticleEffect, MyMesh,
				                                                                                     NameBoneToAttached, Loc, FRotator::ZeroRotator,
				                                                                                     EAttachLocation::SnapToTarget, false);
				ParticleSystemEffects.Add(NewParticleSystem);
			}
		}
	}
	else
	{
		if (ParticleSystemEffects.Num() > 0)
		{
			bool bIsFind = false;
			int32 i = 0;
			while (i < ParticleSystemEffects.Num() && !bIsFind)
			{
				if (ParticleSystemEffects[i] && ParticleSystemEffects[i]->Template && Effect->ParticleEffect && Effect->
				    ParticleEffect == ParticleSystemEffects[i]->Template)
				{
					bIsFind = true;
					ParticleSystemEffects[i]->DeactivateSystem();
					ParticleSystemEffects[i]->DestroyComponent();
					ParticleSystemEffects.RemoveAt(i);
				}
				i++;
			}
		}
	}
}

bool AVandalEnemyCharacter::ReplicateSubobjects(UActorChannel* Channel, FOutBunch* Bunch, FReplicationFlags* RepFlags)
{
	bool Wrote = Super::ReplicateSubobjects(Channel, Bunch, RepFlags);

	for (int32 i = 0; i < Effects.Num(); i++)
	{
		if (Effects[i])
		{
			Wrote |= Channel->ReplicateSubobject(Effects[i], *Bunch, *RepFlags);
		}
	}
	return Wrote;
}

void AVandalEnemyCharacter::GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const
{
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);

	DOREPLIFETIME(AVandalEnemyCharacter, Effects);
	DOREPLIFETIME(AVandalEnemyCharacter, EffectAdd);
	DOREPLIFETIME(AVandalEnemyCharacter, EffectRemove);
}
