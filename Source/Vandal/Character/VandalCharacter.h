// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Character.h"
#include "Vandal/Character/CharacterHealthComponent.h"
#include "Vandal/Character/InventoryComponent.h"
#include "Vandal/FuncLibrary/Types.h"
#include "Vandal/Game/StateEffect.h"
#include "Vandal/Game/Weapon.h"
#include "Vandal/Interface/Vandal_IGameActor.h"

#include "VandalCharacter.generated.h"

UCLASS(Blueprintable)
class AVandalCharacter
	: public ACharacter
	  , public IVandal_IGameActor
{
	GENERATED_BODY()

protected:
	virtual void BeginPlay() override;

	// Inputs
	void InputAxisY(float Value);
	void InputAxisX(float Value);

	void InputAttackPressed();
	void InputAttackReleased();

	void InputWalkPressed();
	void InputWalkReleased();

	void InputSprintPressed();
	void InputSprintReleased();

	void InputAimPressed();
	void InputAimReleased();

	// Inventory Inputs
	void TrySwitchNextWeapon();
	void TrySwitchPreviousWeapon();
	// Ability Inputs
	void TryAbilityEnabled();

	template<int32 Id>
	void TKeyPressed()
	{
		TrySwitchWeaponToIndexByKeyInput_OnServer(Id);
	}

	// Inputs End

	// Input Flags
	float AxisX = 0.0f;
	float AxisY = 0.0f;

	bool SprintRunEnabled = false;
	bool WalkEnabled = false;
	bool AimEnabled = false;

	UPROPERTY(Replicated)
	EMovementState MovementState = EMovementState::Run_State;

	UPROPERTY(Replicated)
	AWeapon* CurrentWeapon = nullptr;

	UPROPERTY()
	UDecalComponent* CurrentCursor = nullptr;

	UPROPERTY(Replicated)
	TArray<UStateEffect*> Effects;
	UPROPERTY(ReplicatedUsing = EffectAdd_OnRep)
	UStateEffect* EffectAdd = nullptr;
	UPROPERTY(ReplicatedUsing = EffectRemove_OnRep)
	UStateEffect* EffectRemove = nullptr;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Debug")
	TArray<UParticleSystemComponent*> ParticleSystemEffects;

	UPROPERTY(Replicated)
	int32 CurrentIndexWeapon = 0;

	UFUNCTION()
	void CharDead();
	UFUNCTION(NetMulticast, Reliable)
	void EnableRagdoll_Multicast();

	virtual float TakeDamage(float DamageAmount, const FDamageEvent& DamageEvent, AController* EventInstigator, AActor* DamageCauser) override;
	virtual bool ReplicateSubobjects(UActorChannel* Channel, FOutBunch* Bunch, FReplicationFlags* RepFlags) override;

public:
	AVandalCharacter();

	FTimerHandle TimerHandle_RagDollTimer;

	// Called every frame.
	virtual void Tick(float DeltaSeconds) override;

	virtual void SetupPlayerInputComponent(UInputComponent* PlayerInputComponent) override;

	/** Returns TopDownCameraComponent subobject **/
	FORCEINLINE class UCameraComponent* GetTopDownCameraComponent() const { return TopDownCameraComponent; }

	/** Returns CameraBoom subobject **/
	FORCEINLINE class USpringArmComponent* GetCameraBoom() const { return CameraBoom; }

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Inventory", meta = (AllowPrivateAccess = "true"))
	UInventoryComponent* InventoryComponent;
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Health", meta = (AllowPrivateAccess = "true"))
	UCharacterHealthComponent* CharHealthComponent;
	// Cursor material on decal
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Cursor")
	UMaterialInterface* CursorMaterial = nullptr;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Cursor")
	FVector CursorSize = FVector(20.0f, 40.0f, 40.0f);
	// Default move rule and state character
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Movement")
	FCharacterSpeed MovementSpeedInfo;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Movement")
	TArray<UAnimMontage*> DeathAnimations;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Ability")
	TSubclassOf<UStateEffect> AbilityEffect;

private:
	/** Top down camera */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera, meta = (AllowPrivateAccess = "true"))
	UCameraComponent* TopDownCameraComponent;
	/** Camera boom positioning the camera above the character */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera, meta = (AllowPrivateAccess = "true"))
	USpringArmComponent* CameraBoom;

public:
	// Tick Func
	UFUNCTION()
	void MovementTick(float DeltaTime);
	// Tick Func End

	// Func
	void CharacterUpdate() const;
	void ChangeMovementState(EMovementState NewMovementState);

	void AttackCharEvent(bool bIsFiring) const;

	UFUNCTION()
	void InitWeapon(FName IdWeaponName, FAdditionalWeaponInfo WeaponAdditionalInfo, int32 NewCurrentIndexWeapon);
	void TryReloadWeapon();
	UFUNCTION()
	void WeaponFire(UAnimMontage* Anim);
	UFUNCTION()
	void WeaponReloadStart(UAnimMontage* Anim);
	UFUNCTION()
	void WeaponReloadEnd(bool bIsSuccess, int32 AmmoTake);
	UFUNCTION(Server, Reliable)
	void TrySwitchWeaponToIndexByKeyInput_OnServer(int32 ToIndex);
	void DropCurrentWeapon();
	UFUNCTION(BlueprintNativeEvent)
	void WeaponReloadStart_BP(UAnimMontage* Anim);
	UFUNCTION(BlueprintNativeEvent)
	void WeaponReloadEnd_BP(bool bIsSuccess);
	UFUNCTION(BlueprintNativeEvent)
	void WeaponFire_BP(UAnimMontage* Anim);

	UFUNCTION(BlueprintCallable, BlueprintPure)
	AWeapon* GetCurrentWeapon() const;
	UFUNCTION(BlueprintCallable, BlueprintPure)
	UDecalComponent* GetCursorToWorld() const;
	UFUNCTION(BlueprintCallable, BlueprintPure)
	EMovementState GetMovementState() const;
	UFUNCTION(BlueprintCallable, BlueprintPure)
	// ReSharper disable once CppUEBlueprintCallableFunctionUnused
	TArray<UStateEffect*> GetCurrentEffectsOnChar();
	UFUNCTION(BlueprintCallable, BlueprintPure)
	int32 GetCurrentWeaponIndex() const;
	UFUNCTION(BlueprintCallable, BlueprintPure)
	bool GetIsAlive() const;
	// Func End

	// Interface
	virtual EPhysicalSurface GetSurfaceType() override;
	virtual TArray<UStateEffect*> GetAllCurrentEffects() override;
	UFUNCTION(BlueprintCallable, BlueprintNativeEvent)
	// ReSharper disable once CppEnforceOverridingFunctionStyle
	// ReSharper disable once CppHidingFunction
	void RemoveEffect(UStateEffect* RemoveEffect);
	virtual void RemoveEffect_Implementation(UStateEffect* RemoveEffect) override;
	UFUNCTION(BlueprintCallable, BlueprintNativeEvent)
	// ReSharper disable once CppEnforceOverridingFunctionStyle
	// ReSharper disable once CppHidingFunction
	void AddEffect(UStateEffect* NewEffect);
	virtual void AddEffect_Implementation(UStateEffect* NewEffect) override;
	// End Interface

	UFUNCTION(BlueprintNativeEvent)
	void CharDead_BP();

	UFUNCTION(Server, Unreliable)
	void SetActorRotationByYaw_OnServer(float Yaw);
	UFUNCTION(NetMulticast, Unreliable)
	void SetActorRotationByYaw_Multicast(float Yaw);

	UFUNCTION(Server, Reliable)
	void SetMovementState_OnServer(EMovementState NewState);
	UFUNCTION(NetMulticast, Reliable)
	void SetMovementState_Multicast(EMovementState NewState);

	UFUNCTION(Server, Reliable)
	void TryReloadWeapon_OnServer();

	UFUNCTION(NetMulticast, Reliable)
	void PlayAnim_Multicast(UAnimMontage* Anim);

	UFUNCTION()
	void EffectAdd_OnRep();
	UFUNCTION()
	void EffectRemove_OnRep();

	UFUNCTION()
	void SwitchEffect(const UStateEffect* Effect, bool bIsAdd);

	UFUNCTION(Server, Reliable)
	void ExecuteEffectAdded_OnServer(UParticleSystem* ParticleSystem);
	UFUNCTION(NetMulticast, Reliable)
	void ExecuteEffectAdded_Multicast(UParticleSystem* ParticleSystem);
};
