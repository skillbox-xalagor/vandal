// Fill out your copyright notice in the Description page of Project Settings.


#include "VandalGameInstance.h"
#include "GameFramework/GameUserSettings.h"

bool UVandalGameInstance::GetWeaponInfoByName(const FName NameWeapon, FWeaponInfo& OutInfo) const
{
	bool bIsFind = false;

	if (WeaponInfoTable)
	{
		const FWeaponInfo* WeaponInfoRow = WeaponInfoTable->FindRow<FWeaponInfo>(NameWeapon, "", false);
		if (WeaponInfoRow)
		{
			bIsFind = true;
			OutInfo = *WeaponInfoRow;
		}
	}
	else
	{
		UE_LOG(LogTemp, Warning, TEXT("UVandalGameInstance::GetWeaponInfoByName - WeaponTable -NULL"));
	}

	return bIsFind;
}

bool UVandalGameInstance::GetDropItemInfoByWeaponName(const FName NameItem, FDropItem& OutInfo) const
{
	bool bIsFind = false;

	if (DropItemInfoTable)
	{
		TArray<FName> RowNames = DropItemInfoTable->GetRowNames();

		int8 i = 0;
		while (i < RowNames.Num() && !bIsFind)
		{
			const FDropItem* DropItemInfoRow = DropItemInfoTable->FindRow<FDropItem>(RowNames[i], "");
			if (DropItemInfoRow->WeaponInfo.NameItem == NameItem)
			{
				OutInfo = *DropItemInfoRow;
				bIsFind = true;
			}
			i++; //fix
		}
	}
	else
	{
		UE_LOG(LogTemp, Warning, TEXT("UVandalGameInstance::GetDropItemInfoByName - DropItemInfoTable -NULL"));
	}

	return bIsFind;
}

bool UVandalGameInstance::GetDropItemInfoByName(const FName NameItem, FDropItem& OutInfo) const
{
	bool bIsFind = false;

	if (DropItemInfoTable)
	{
		const FDropItem* DropItemInfoRow = DropItemInfoTable->FindRow<FDropItem>(NameItem, "", false);
		if (DropItemInfoRow)
		{
			bIsFind = true;
			OutInfo = *DropItemInfoRow;
		}
	}
	else
	{
		UE_LOG(LogTemp, Warning, TEXT("UVandalGameInstance::GetWeaponInfoByName - WeaponTable -NULL"));
	}

	return bIsFind;
}

void UVandalGameInstance::Init()
{
	Super::Init();

	GEngine->GameUserSettings->SetVSyncEnabled(true);
	GEngine->GameUserSettings->SetFullscreenMode(EWindowMode::WindowedFullscreen);
	GEngine->GameUserSettings->ApplySettings(true);
	GEngine->GameUserSettings->SaveSettings();
	GEngine->Exec(GetWorld(), TEXT("t.maxFPS 60"));
}
